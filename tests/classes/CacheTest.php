<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes;

use JLanger\Cache\classes\Cache;
use JLanger\Cache\classes\CacheObj;
use JLanger\Cache\classes\Configs\GeneralCacheConfig;
use JLanger\Cache\Interfaces\CacheMethodInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CacheTest extends TestCase
{
    /**
     * @var GeneralCacheConfig|MockObject
     */
    private $config;

    /**
     * @var Cache
     */
    private $subject;

    /**
     * @var CacheMethodInterface|MockObject
     */
    private $cache;

    public function setUp(): void
    {
        $this->cache = $this->createMock(CacheMethodInterface::class);

        $this->config = $this->createMock(GeneralCacheConfig::class);
        $this->config->method('getCacheMethod')->willReturn($this->cache);
        $this->config->method('getPrefix')->willReturn('test_');

        $this->subject = new Cache($this->config);
    }

    public function testRead(): void
    {
        $result = new CacheObj('testing');

        $this->cache
            ->expects($this->once())
            ->method('read')
            ->with('test_900150983cd24fb0d6963f7d28e17f72')
            ->willReturn($result);

        $this->assertSame($this->subject->read('abc'), $result);
    }

    public function testWrite(): void
    {
        $this->cache
            ->expects($this->once())
            ->method('write')
            ->with('test_900150983cd24fb0d6963f7d28e17f72', 'bananas', null);

        $this->subject->write('abc', 'bananas');
    }

    public function testDelete(): void
    {
        $this->cache
            ->expects($this->once())
            ->method('delete')
            ->with('test_900150983cd24fb0d6963f7d28e17f72');

        $this->subject->delete('abc');
    }

    public function testClear(): void
    {
        $this->cache
            ->expects($this->once())
            ->method('clear');

        $this->subject->clear();
    }

    public function testErrorCase(): void
    {
        $this->expectNotice();

        $this->cache
            ->expects($this->once())
            ->method('clear')
            ->willThrowException(new \InvalidArgumentException('Failed to clear cache'));

        $this->subject->clear();
    }
}
