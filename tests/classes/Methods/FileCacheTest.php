<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes\Methods;

use JLanger\Cache\classes\Configs\FileCacheConfig;
use JLanger\Cache\classes\Methods\FileCache;
use PHPUnit\Framework\TestCase;

class FileCacheTest extends TestCase
{
    private const TEST_DIR = __DIR__.'/tests/';

    /**
     * @var FileCache
     */
    private FileCache $subject;

    protected function setUp(): void
    {
        $this->subject = new FileCache(new FileCacheConfig(self::TEST_DIR));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        shell_exec(sprintf('rm -rf "%s"', self::TEST_DIR));
    }

    public function testWrite(): void
    {
        $this->subject->write('test', 'value');

        $this->assertFileExists(self::TEST_DIR.'test');
    }

    public function testRead(): void
    {
        $this->subject->write('test', 'value');

        $this->assertTrue($this->subject->read('test')->hasValue());
        $this->assertSame($this->subject->read('test')->getValue(), 'value');
    }


    public function testReadWithEndOfLifeTime(): void
    {
        $this->subject->write('test', 'value', 1);
        sleep(3);

        $this->assertFalse($this->subject->read('test')->hasValue());
    }

    public function testDelete(): void
    {
        $this->subject->write('test', 'value');
        $this->subject->delete('test');

        $this->assertFileNotExists(self::TEST_DIR.'test');
    }

    public function testClear(): void
    {
        $config = new FileCacheConfig(self::TEST_DIR);
        $config->setPrefix('abc');
        $subject = new FileCache($config);

        $subject->write('test', 'value');
        $subject->write('abc_bananas', 'value');
        $subject->clear();

        $this->assertFileExists(self::TEST_DIR.'test');
        $this->assertFileNotExists(self::TEST_DIR.'abc_bananas');
    }
}
