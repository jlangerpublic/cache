<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes\Methods;

use JLanger\Cache\classes\Configs\RedisCacheConfig;
use JLanger\Cache\classes\Methods\Redis;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Predis\ClientInterface;

class RedisTest extends TestCase
{
    
    private Redis $subject;

    /** @var MockObject|ClientInterface */
    private $redisClient;

    protected function setUp(): void
    {
        $this->redisClient = $mockClient = $this->createMock(ClientInterface::class);

        $redisConfig = $this->getMockBuilder(RedisCacheConfig::class)
                            ->disableOriginalConstructor()
                            ->getMock();
        $redisConfig->method('getClient')->willReturn($mockClient);
        $redisConfig->method('getLifeTime')->willReturn(100);

        $this->subject = new Redis($redisConfig);
    }

    public function testRead(): void
    {
        $this->redisClient
            ->expects($this->once())
            ->method('__call')
            ->with('exists', ['some_key'])
            ->willReturn(false);

        $this->assertFalse($this->subject->read('some_key')->hasValue());
    }

    public function testReadSuccessful(): void
    {
        $this->redisClient
            ->method('__call')
            ->withConsecutive(
                ['exists', ['some_key']],
                ['get', ['some_key']]
            )
            ->willReturnOnConsecutiveCalls(true, 12);

        $cacheObj = $this->subject->read('some_key');
        $this->assertTrue($cacheObj->hasValue());
        $this->assertSame($cacheObj->getValue(), 12);
    }

    public function testWrite(): void
    {
        $this->redisClient
            ->expects($this->once())
            ->method('__call')
            ->with('set', ['some_key', 'some_value', null, 100]);

        $this->subject->write('some_key', 'some_value');
    }

    public function testWriteWithLifeTime(): void
    {
        $this->redisClient
            ->expects($this->once())
            ->method('__call')
            ->with('set', ['some_key', 'some_value', null, 10]);

        $this->subject->write('some_key', 'some_value', 10);
    }

    public function testDelete(): void
    {
        $this->redisClient
            ->expects($this->once())
            ->method('__call')
            ->with('del', [['some_key']]);

        $this->subject->delete('some_key');
    }

    public function testClear(): void
    {
        $keys = ['some_key', 'some_flower'];
        $this->redisClient
            ->expects(self::atLeast(2))
            ->method('__call')
            ->withConsecutive(
                ['keys', ['*']],
                ['del', [$keys]]
            )
            ->willReturnOnConsecutiveCalls($keys, null);

        $this->subject->clear();
    }
}
