<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes\Configs;

use JLanger\Cache\classes\Configs\GeneralCacheConfig;
use PHPUnit\Framework\TestCase;

class GeneralCacheConfigTest extends TestCase
{

    private GeneralCacheConfig $subject;

    protected function setUp(): void
    {
        $this->subject = new GeneralCacheConfig();
    }

    public function testUsingConstructorArguments(): void
    {
        $subject = new GeneralCacheConfig(10, 'abc_');

        $this->assertSame($subject->getLifetime(), 10);
        $this->assertSame($subject->getPrefix(), 'abc_');
    }

    public function testDefaultValues(): void
    {
        $this->assertSame($this->subject->getLifetime(), 86400);
        $this->assertSame($this->subject->getPrefix(), '');
    }

    public function testItDoesNotAllowNegativeLifeTime(): void
    {
        $this->subject->setLifetime(-1);

        $this->assertSame($this->subject->getLifetime(), 86400);
    }

    public function testAddingUnderscoreToPrefix(): void
    {
        $this->subject->setPrefix('abc');

        $this->assertSame($this->subject->getPrefix(), 'abc_');
    }
}
