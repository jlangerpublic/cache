<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes\Configs;

use JLanger\Cache\classes\Configs\APCCacheConfig;
use JLanger\Cache\classes\Methods\APC;
use JLanger\Cache\classes\Methods\APCu;
use PHPUnit\Framework\TestCase;

class APCCacheConfigTest extends TestCase
{
    
    private APCCacheConfig $subject;

    public function setup(): void
    {
        $this->subject = new APCCacheConfig();
    }

    public function testNoCacheInstalled(): void
    {
        $this->assertInstanceOf(APCu::class, $this->subject->getCacheMethod());
    }

    public function testGeneralConfig(): void
    {
        $this->assertSame($this->subject->getLifetime(), 86400);
        $this->assertSame($this->subject->getPrefix(), '');
    }
}
