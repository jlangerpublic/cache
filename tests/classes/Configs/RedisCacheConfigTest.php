<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes\Configs;

use JLanger\Cache\classes\Configs\RedisCacheConfig;
use JLanger\Cache\classes\Methods\Redis;
use PHPUnit\Framework\TestCase;

class RedisCacheConfigTest extends TestCase
{
    private RedisCacheConfig $subject;

    public function setup(): void
    {
        $this->subject = new RedisCacheConfig();
    }

    public function testDefaultValues(): void
    {
        $this->assertSame($this->subject->hostname, 'localhost');
        $this->assertSame($this->subject->port, 6379);
        $this->assertSame($this->subject->password, '');
    }

    public function testSelfDefinedDirectory(): void
    {
        $this->subject->hostname = '127.0.0.1';
        $this->subject->port     = 123;
        $this->subject->password = 'secret';
        $this->assertSame($this->subject->hostname, '127.0.0.1');
        $this->assertSame($this->subject->port, 123);
        $this->assertSame($this->subject->password, 'secret');
    }

    public function testGeneralConfig(): void
    {
        $this->assertSame($this->subject->getLifetime(), 86400);
        $this->assertSame($this->subject->getPrefix(), '');
    }

    public function testGettingCacheMethod(): void
    {
        $this->assertInstanceOf(Redis::class, $this->subject->getCacheMethod());
    }
}
