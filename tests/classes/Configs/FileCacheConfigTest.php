<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes\Configs;

use JLanger\Cache\classes\Configs\FileCacheConfig;
use JLanger\Cache\classes\Methods\FileCache;
use PHPUnit\Framework\TestCase;

class FileCacheConfigTest extends TestCase
{
    
    private FileCacheConfig $subject;

    public function setup(): void
    {
        $this->subject = new FileCacheConfig();
    }

    public function testDefaultDirectory(): void
    {
        $this->assertSame($this->subject->getDirectory(), 'filecache/');
    }

    public function testSelfDefinedDirectory(): void
    {
        $this->subject->setDirectory('some_folder/');
        $this->assertSame($this->subject->getDirectory(), 'some_folder/');
    }

    public function testAddingASlashAtTheEnd(): void
    {
        $this->subject->setDirectory('abc');
        $this->assertSame($this->subject->getDirectory(), 'abc/');
    }

    public function testGeneralConfig(): void
    {
        $this->assertSame($this->subject->getLifetime(), 86400);
        $this->assertSame($this->subject->getPrefix(), 'filecache_');
    }

    public function testGettingMethod(): void
    {
        $this->assertInstanceOf(FileCache::class, $this->subject->getCacheMethod());
    }
}
