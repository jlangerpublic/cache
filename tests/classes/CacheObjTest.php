<?php
declare(strict_types=1);

namespace Tests\JLanger\Cache\classes;

use JLanger\Cache\classes\Cache;
use JLanger\Cache\classes\CacheObj;
use JLanger\Cache\classes\Configs\GeneralCacheConfig;
use JLanger\Cache\Interfaces\CacheMethodInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CacheObjTest extends TestCase
{
    public function testHasValue(): void
    {
        $obj = new CacheObj('abc');

        $this->assertTrue($obj->hasValue());
        $this->assertSame($obj->getValue(), 'abc');
    }
    public function testHasNoValue(): void
    {
        $obj = new CacheObj(null, false);

        $this->assertFalse($obj->hasValue());
    }
    public function testHasNoValueAndAsserts(): void
    {
        $obj = new CacheObj(null, false);

        $this->assertNull($obj->getValue());
    }
}
