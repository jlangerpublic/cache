<?php
declare(strict_types=1);

namespace JLanger\Cache\Interfaces;

use Exception;
use JLanger\Cache\classes\CacheObj;

interface CacheMethodInterface
{
    /** get content from Cache.
     *
     * @param string $key
     *
     * @return mixed
     * @throws Exception
     */
    public function read(string $key): CacheObj;

    public function write(string $key, $content, ?int $lifetime = null): void;

    public function delete(string $key): void;

    public function clear(): void;
}
