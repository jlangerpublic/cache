<?php
declare(strict_types=1);

namespace JLanger\Cache\Interfaces;

use JLanger\Cache\classes\Configs\GeneralCacheConfig;

interface CacheInterface extends CacheMethodInterface
{
    public function __construct(GeneralCacheConfig $config);
}
