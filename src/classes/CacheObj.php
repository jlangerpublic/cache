<?php
declare(strict_types=1);

namespace JLanger\Cache\classes;

class CacheObj
{
    private bool $hasValue;
    
    /** @var mixed */
    private $value;

    public function __construct($value, bool $hasValue = true)
    {
        $this->value    = $value;
        $this->hasValue = $hasValue;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        assert($this->hasValue);
        return $this->value;
    }

    /**
     * @return bool
     */
    public function hasValue(): bool
    {
        return $this->hasValue;
    }
}
