<?php
declare(strict_types=1);

namespace JLanger\Cache\classes;

use Exception;
use JLanger\Cache\classes\Configs\FileCacheConfig;
use JLanger\Cache\classes\Configs\GeneralCacheConfig;
use JLanger\Cache\Interfaces\CacheInterface;
use JLanger\Cache\Interfaces\CacheMethodInterface;

class Cache implements CacheInterface
{
    private GeneralCacheConfig $config;
    private CacheMethodInterface $cache;

    public function __construct(GeneralCacheConfig $config)
    {
        $this->config = $config;
        try {
            $this->cache = $config->getCacheMethod();
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            $this->cache = (new FileCacheConfig())->getCacheMethod();
        }
    }

    /**
     * @inheritDoc
     */
    public function read(string $key): CacheObj
    {
        return $this->cache->read($this->getHashedKey($key));
    }

    public function write(string $key, $content, ?int $lifetime = null): void
    {
        try {
            $this->cache->write(
                $this->getHashedKey($key),
                $content,
                $lifetime
            );
        } catch (Exception $e) {
            trigger_error($e->getMessage());
        }
    }

    public function delete(string $key): void
    {
        try {
            $this->cache->delete($this->getHashedKey($key));
        } catch (Exception $e) {
            trigger_error($e->getMessage());
        }
    }

    public function clear(): void
    {
        try {
            $this->cache->clear();
        } catch (Exception $e) {
            trigger_error($e->getMessage());
        }
    }

    private function getHashedKey(string $key): string
    {
        return $this->config->getPrefix().md5($key);
    }
}
