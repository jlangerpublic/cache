<?php

declare(strict_types=1);

namespace JLanger\Cache\classes\Methods;

use JLanger\Cache\classes\CacheObj;
use JLanger\Cache\classes\Configs\RedisCacheConfig;
use JLanger\Cache\Interfaces\CacheMethodInterface;
use Predis\ClientInterface;

class Redis implements CacheMethodInterface
{
    
    private ClientInterface $redis;
    
    private RedisCacheConfig $config;

    public function __construct(RedisCacheConfig $config)
    {
        $this->config = $config;
        $this->redis  = $config->getClient();
    }

    public function read(string $key): CacheObj
    {
        $hasValue = (bool)$this->redis->exists($key);
        $value    = $hasValue ? $this->redis->get($key) : null;
        return new CacheObj($value, $hasValue);
    }

    public function write(string $key, $content, ?int $lifetime = null): void
    {
        $lifetime = $lifetime ?? $this->config->getLifetime();
        $this->redis->set($key, $content, null, $lifetime);
    }
    public function delete(string $key): void
    {
        $this->redis->del([$key]);
    }
    public function clear(): void
    {
        $prefix = $this->config->getPrefix() . '*';
        $this->redis->del($this->redis->keys($prefix));
    }
}
