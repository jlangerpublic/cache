<?php
declare(strict_types=1);

namespace JLanger\Cache\classes\Methods;

use Exception;
use JLanger\Cache\classes\CacheObj;
use JLanger\Cache\classes\Configs\FileCacheConfig;
use JLanger\Cache\Interfaces\CacheMethodInterface;

class FileCache implements CacheMethodInterface
{

    private FileCacheConfig $config;

    public function __construct(FileCacheConfig $config)
    {
        $this->config = $config;
    }
    public function read(string $key): CacheObj
    {
        $filename = $this->config->getDirectory() . $key;
        $hasValue = file_exists($filename);
        $value    = null;
        if ($hasValue) {
            $data = unserialize(file_get_contents($filename));
            if ($data['lifetime'] === 0 || (time() - filemtime($filename)) < $data['lifetime']) {
                $value = $data['value'];
            } else {
                $this->delete($key);
                $hasValue = false;
            }
        }
        return new CacheObj($value, $hasValue);
    }

    /**
     * @param string   $key
     * @param          $content
     * @param int|null $lifetime
     *
     * @throws Exception
     */
    public function write(string $key, $content, ?int $lifetime = null): void
    {
        $dir = $this->config->getDirectory();
        if (!is_dir($dir) && mkdir($dir, 0777, true) === false) {
            throw new Exception('Could not add cache-directory.');
        }

        $file = $this->config->getDirectory() . $key;
        if (file_exists($file)) {
            $this->delete($key);
        }
        $lifetime = $lifetime ?? $this->config->getLifetime();
        file_put_contents(
            $file,
            serialize(
                [
                    'value'    => $content,
                    'lifetime' => $lifetime
                ]
            )
        );
    }

    /**
     * @param string $key
     *
     * @throws Exception
     */
    public function delete(string $key): void
    {
        $file = $this->config->getDirectory() . $key;
        if (is_file($file)) {
            unlink($file);
        } else {
            throw new Exception("File does not exist.");
        }
    }

    /**
     * @throws Exception
     */
    public function clear(): void
    {
        $scan = scandir($this->config->getDirectory());
        foreach ($scan as $file) {
            if (is_file($this->config->getDirectory() . $file) && strpos($file, $this->config->getPrefix()) === 0) {
                $this->delete($file);
            }
        }
    }
}
