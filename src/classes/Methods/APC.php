<?php
declare(strict_types=1);

namespace JLanger\Cache\classes\Methods;

use JLanger\Cache\classes\CacheObj;
use JLanger\Cache\classes\Configs\APCCacheConfig;
use JLanger\Cache\Interfaces\CacheMethodInterface;

class APC implements CacheMethodInterface
{
    private APCCacheConfig $config;

    public function __construct(APCCacheConfig $config)
    {
        $this->config = $config;
    }

    public function read(string $key): CacheObj
    {
        $hasValue = apc_exists($key);
        $value    = $hasValue ? apc_fetch($key) : null;
        return new CacheObj($value, $hasValue);
    }

    public function write(string $key, $content, ?int $lifetime = null): void
    {
        $lifetime = $lifetime ?? $this->config->getLifetime();
        apc_add($key, $content, $lifetime);
    }

    public function delete(string $key): void
    {
        apc_delete($key);
    }

    public function clear(): void
    {
        foreach (apc_cache_info()['cache_list'] as $item) {
            if (strpos($item['info'], $this->config->getPrefix()) === 0) {
                apc_delete($item['info']);
            }
        }
    }
}
