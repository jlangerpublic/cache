<?php
declare(strict_types=1);

namespace JLanger\Cache\classes\Configs;

use JLanger\Cache\Interfaces\CacheMethodInterface;

abstract class GeneralCacheConfig
{
    private const DEFAULT_LIFETIME = 86400;

    private ?int $lifetime;
    private ?string $prefix;

    public function __construct(?int $lifetime = null, ?string $prefix = null)
    {
        $this->setLifetime($lifetime ?? self::DEFAULT_LIFETIME);
        $this->setPrefix($prefix ?? '');
    }

    public function getLifetime(): int
    {
        return $this->lifetime;
    }

    public function setLifetime(int $lifetime): void
    {
        $this->lifetime = $lifetime >= 0 ? $lifetime : self::DEFAULT_LIFETIME;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function setPrefix(string $prefix): void
    {
        $this->prefix = strlen($prefix) > 0 ? rtrim($prefix, '_') . '_' : '';
    }

    abstract public function getCacheMethod(): CacheMethodInterface;
}
