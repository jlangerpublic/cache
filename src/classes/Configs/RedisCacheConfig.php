<?php
declare(strict_types=1);

namespace JLanger\Cache\classes\Configs;

use JLanger\Cache\classes\Methods\Redis;
use JLanger\Cache\Interfaces\CacheMethodInterface;
use Predis\Client;
use Predis\ClientInterface;

class RedisCacheConfig extends GeneralCacheConfig
{
    public string $hostname = 'localhost';
    public int $port = 6379;
    public string $password = '';

    public function getClient(): ClientInterface
    {

        $connectArr = [
            "scheme" => "tcp",
            "host"   => $this->hostname,
            "port"   => $this->port,
        ];
        if (strlen($this->password) > 0) {
            $connectArr["password"] = $this->password;
        }

        return new Client($connectArr);
    }

    public function getCacheMethod(): CacheMethodInterface
    {
        return new Redis($this);
    }
}
