<?php
declare(strict_types=1);

namespace JLanger\Cache\classes\Configs;

use JLanger\Cache\classes\Methods\FileCache;
use JLanger\Cache\Interfaces\CacheMethodInterface;

class FileCacheConfig extends GeneralCacheConfig
{
    
    private ?string $dir;

    public function __construct(?string $dir = null, ?int $lifetime = null, ?string $prefix = null)
    {
        $prefix = $prefix ?? 'filecache';
        parent::__construct($lifetime, $prefix);
        $this->setDirectory($dir ?? 'filecache/');
    }

    public function getDirectory(): string
    {
        return $this->dir;
    }

    public function setDirectory(string $dir): void
    {
        $this->dir = rtrim($dir, '/') . '/';
    }

    public function getCacheMethod(): CacheMethodInterface
    {
        return new FileCache($this);
    }
}
