<?php
declare(strict_types=1);


namespace JLanger\Cache\classes\Configs;

use Exception;
use JLanger\Cache\classes\Methods\APC;
use JLanger\Cache\classes\Methods\APCu;
use JLanger\Cache\Interfaces\CacheMethodInterface;

class APCCacheConfig extends GeneralCacheConfig
{

    /**
     * @return CacheMethodInterface
     * @throws Exception
     */
    public function getCacheMethod(): CacheMethodInterface
    {
        $is_apc_installed  = function_exists('apc_add');
        $is_apcu_installed = function_exists('apcu_add');
        if ($is_apcu_installed) {
            return new APCu($this);
        } elseif ($is_apc_installed) {
            return new APC($this);
        }
        throw new Exception("neither apc or apcu is available.");
    }
}
